# Project Title-
Users Info app

## Description
Users Info app is the Frontend  project connected with backend project to maintain users information like name,Email,age and gender.It includes the javascript,HTML,CSS using react-redux framework. This project is implemented by using ASyncThunk method, Extrareducer,useDispatch,useSelector,provider Functions.


# Developed by-
Vivek Mahaveer Sapate
Email:viveksapate10@gmail.com
Phone:+91-7411548299

## Getting Started

### Installing

* Clone this source file (https://gitlab.com/viveksapate10/users-crud-react-frontend-project-connected-with-backend.git) into local machine and Open in visual studio code.

### Pre-requisite

*For running this project ,it is mandatory to run the backend project(https://gitlab.com/viveksapate10/users-crud-backend.git), Refer this project with README.md file

### Dependencies
Packages ,that are already installed in this project

*Redux Toolkit

*Bootstrap

*Router DOM

*React-Router-DOM

*CORS

Note-No need to install other packages to run this project.

### Executing program

* After opening this project go to >>View >>Terminal

* After Opening the Terminal ,write a command>> **npm start** <<>>


# Challenges Faced
*Difficulty to use CORS Policy.


## Help

Any advise for common problems or issues.
command to run if program contains helper info

# FAQ's
* Do you have a data store?
*ANSWER-No Data Store.Used public data.


* What is left?
*ANSWER-Multi-factor authentication will be implementing using Microsoft Entra ID.

